#!/usr/bin/env bash

#ln -fs /own-directory /vagrant/directory

#rm -rf /var/www
#ln -fs /hosts /var/www

apt-get update
sudo apt-get -y install nodejs
sudo apt-get -y install npm

mkdir /www

sudo apt-get install gnupg

wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc > /dev/null
sudo apt-key add - 

echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" > /dev/null
sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

sudo apt-get install -y mongodb-org

sudo systemctl daemon-reload

sudo systemctl start mongod

